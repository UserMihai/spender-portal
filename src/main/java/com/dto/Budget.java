package com.dto;

public class Budget {

    private int id;

    private int number;

    public Budget() {
    }

    public Budget(final int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Budget{" +
                "id=" + id +
                ", number=" + number +
                '}';
    }
}
