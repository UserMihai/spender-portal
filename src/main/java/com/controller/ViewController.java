package com.controller;

import com.dto.Budget;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


@Controller
public class ViewController {

    private RestTemplate restTemplate;

    @Value("${welcome.message:test}")
    private String message = "Hello World";

    @RequestMapping(value = "/home")
    public String home() {
        return "index.html";
    }

    @RequestMapping(value = "/budget")
    public String showBudget(final ModelMap modelMap) {
        restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        final Budget[] budget = restTemplate.getForObject("http://localhost:9001/budget", Budget[].class);
        modelMap.addAttribute("budget", budget);
        return "budget.html";
    }

    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        model.put("message", this.message);
        return "welcome";
    }
}
